import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import * as firebase from 'firebase';
import * as Facebook from 'expo-facebook';
import * as Google from 'expo-google-app-auth';


const firebaseConfig = {
  apiKey: "AIzaSyBlqiYf4Ib9euyH03vbcvRLCRWdlKMDEgc",
  authDomain: "digital-menu-2020.firebaseapp.com",
  databaseURL: "https://digital-menu-2020.firebaseio.com",
  projectId: "digital-menu-2020",
  storageBucket: "digital-menu-2020.appspot.com",
  messagingSenderId: "788899781608",
  appId: "1:788899781608:web:a40c492bdde588c56e625d",
  measurementId: "G-GJXR7B25SE"
}

firebase.initializeApp(firebaseConfig);


import { Container, Content, Header, Form, Input, Item, Button, Label } from 'native-base';


export default class App extends React.Component{
  constructor(props){
    super(props)

    this.state = ({
      email:'',
      password:''
    })
  }

  componentDidMount(){
    firebase.auth().onAuthStateChanged((user) => {
      if (user != null){
        console.log(user) 
      }
    })
  }

  signUpUser = (email,password) =>{
    try{
      if(this.state.password.length < 6){
        alert("Please enter atleast 6 characters")
        return;
      }

      firebase.auth().createUserWithEmailAndPassword(email, password)

    }catch(error){
      console.log(error.toString())
    }
  }

  loginUser = (email, password) => {
    try{
      firebase.auth().signInWithEmailAndPassword(email, password).then(function(user){
        console.log(user)
      })
    }catch{
      console.log(error.toString())
    }

  }

  async logInWithFacebook() {
    try {
      await Facebook.initializeAsync({
        appId: '760753964749019',
      });
      const {type,token} = await Facebook.logInWithReadPermissionsAsync({
        permissions: ['email', 'public_profile'],
      });
      if (type === 'success') {
        const credential = firebase.auth.FacebookAuthProvider.credential(token)

        firebase.auth().signInWithCredential(credential).catch((error) => {
          console.log(error)
        })
      }
    } catch ({ message }) {
      alert(`Facebook Login Error: ${message}`);
    }
  }


//Connection with google

  logInWithGoogle = async() => {
    try {
      const result = await Google.logInAsync({
        //androidClientId: YOUR_CLIENT_ID_HERE,
        behavior:'web',
        androidClientId: 'YOUR_CLIENT_ID_HERE',
        iosClientId: '788899781608-pnv87rdvka0qlnpjh8dl9thprvcdfnb8.apps.googleusercontent.com',
        scopes: ['profile', 'email'],
      });
  
      if (result.type === 'success') {
        return result.accessToken;
      } else {
        return { cancelled: true };
      }
    } catch (e) {
      return { error: true };
    }
  }

  


  render(){
    return (
      <Container style={styles.container}>
          <Form>
            <Item floatingLabel>
              <Label> Email </Label>
              <Input
                autoCorrect={false}
                autoCapitalize="none"
                onChangeText={(email) => this.setState({email})}
              />
            </Item>

            <Item floatingLabel>
              <Label> Password </Label>
              <Input
                secureTextEntry={true}
                autoCorrect={false}
                autoCapitalize="none"
                onChangeText={(password) => this.setState({password})}
              />
            </Item>

            <Button style={{ marginTop: 10}}
              full
              rounded
              success
              onPress={()=> this.loginUser(this.state.email,this.state.password)}
            >
              <Text style={{color: 'white'}}> Login </Text>
            </Button>

            <Button style={{ marginTop: 10 }}
              full
              rounded
              primary
              onPress={()=> this.signUpUser(this.state.email,this.state.password)}
            >
              <Text style={{color: 'white'}}> Sign up </Text>
            </Button>

            <Button style={{ marginTop: 10 }}
              full
              rounded
              primary
              onPress={()=> this.logInWithFacebook()}
            >
              <Text style={{color: 'white'}}> Login with Facebook </Text>
            </Button>

            <Button style={{ marginTop: 10 }}
              full
              rounded
              primary
              onPress={()=> this.logInWithGoogle()}
            >
              <Text style={{color: 'white'}}> Login with Gmail </Text>
            </Button>

            <Button style={{ marginTop: 10 }}
              full
              rounded
              primary
              onPress={()=> this.logInWithGoogle()}
            >
              <Text style={{color: 'white'}}> Login with Apple </Text>
            </Button>

          </Form>
      </Container>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
    padding: 10,
  },
});
